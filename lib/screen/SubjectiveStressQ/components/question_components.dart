import 'package:flutter/material.dart';
import 'package:guardian/screen/SubjectiveStressQ/components/radiooption_components.dart';
import 'package:guardian/screen/SubjectiveStressQ/stresssubjque_screen.dart';
import 'package:guardian/utility/constants.dart';

class StressQuestionComp extends StatefulWidget {
  final List<StressQuestion> questionList;
  final int index;
  final Function onChangeRadio;

  StressQuestionComp({this.questionList, this.index, this.onChangeRadio});

  @override
  _StressQuestionCompState createState() => _StressQuestionCompState();
}

class _StressQuestionCompState extends State<StressQuestionComp> {
  Constants constants;
  @override
  Widget build(BuildContext context) {
    constants = new Constants(context);
    List<StressQuestion> questinoList = widget.questionList;
    return Container(
      width: constants.screenWidth,
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 5.0,horizontal: 15.0),
                child: Text(
                      questinoList[widget.index].question,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              RadioOption(
                  optionValue: questinoList[widget.index].option1.optiontext,
                  value: 0,
                  selectedOptionValue: questinoList[widget.index].selctedvalue,
                  onChangeRadio: widget.onChangeRadio),
              RadioOption(
                  optionValue: questinoList[widget.index].option2.optiontext,
                  value: 1,
                  selectedOptionValue: questinoList[widget.index].selctedvalue,
                  onChangeRadio: widget.onChangeRadio),
              RadioOption(
                  optionValue: questinoList[widget.index].option3.optiontext,
                  value: 2,
                  selectedOptionValue: questinoList[widget.index].selctedvalue,
                  onChangeRadio: widget.onChangeRadio),
              RadioOption(
                  optionValue: questinoList[widget.index].option4.optiontext,
                  value: 3,
                  selectedOptionValue: questinoList[widget.index].selctedvalue,
                  onChangeRadio: widget.onChangeRadio),
              RadioOption(
                  optionValue: questinoList[widget.index].option5.optiontext,
                  value: 4,
                  selectedOptionValue: questinoList[widget.index].selctedvalue,
                  onChangeRadio: widget.onChangeRadio),
            ],
          ),
        ),
      ),
    );
  }
}
