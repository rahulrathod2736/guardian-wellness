import 'dart:convert';

import 'package:flutter/widgets.dart';
import 'package:guardian/screen/SubjectiveStressQ/stresssubjque_screen.dart';
import 'package:guardian/utility/sharedPref.dart';

import 'apiconstants.dart';
import 'package:http/http.dart' as http;

class Constants {
  double screenWidth, screenHeight;
  SharedPref sharedPref = new SharedPref();

  Constants(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;
    screenHeight = MediaQuery.of(context).size.height;
  }

  Future<Map<String, dynamic>> postApiCallback(
      String apiPath, Object body) async {
    await sharedPref.initSF();
    String token = sharedPref.getParticipateTokentoSF();

    var url = Uri.http(ApiConstants.apiDomain, apiPath);

    var response = await http.post(
      url,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ' + token,
      },
      body: jsonEncode(body),
    );
    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      return null;
    }
  }

  int getScore(StressQuestion element) {
    int score = 0;
    switch (element.selctedvalue + 1) {
      case 1:
        score = element.option1.optionScore;
        break;
      case 2:
        score = element.option2.optionScore;
        break;
      case 3:
        score = element.option3.optionScore;
        break;
      case 4:
        score = element.option4.optionScore;
        break;
      case 5:
        score = element.option5.optionScore;
        break;
    }
    return score;
  }

  int getTotalScore(List<StressQuestion> qList) {
    int totalScore = 0;
    qList.asMap().forEach((index, element) {
      int getQuestionScore = getScore(element);

      totalScore += getQuestionScore;
    });

    return totalScore;
  }
}
