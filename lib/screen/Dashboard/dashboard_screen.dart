import 'package:flutter/material.dart';
import 'package:guardian/screen/Dashboard/components/menu_components.dart';
import 'package:guardian/screen/Dashboard/dashboard_constants.dart';
import 'package:guardian/screen/HRVRecord/hrvrecord_screen.dart';
import 'package:guardian/screen/RHRRecord/rhrrecord_screen.dart';
import 'package:guardian/screen/Remediation/remediation_screen.dart';
import 'package:guardian/screen/SleepRecord/sleeprecord_screen.dart';
import 'package:guardian/screen/SubjectiveStressQ/stresssubjque_screen.dart';
import 'package:guardian/utility/apiconstants.dart';
import 'package:guardian/utility/components/TrendChart/trendchart_screen.dart';
import 'package:guardian/utility/constants.dart';
import 'package:guardian/utility/sharedPref.dart';
import 'package:intl/intl.dart';

class DashBoardScreen extends StatefulWidget {
  static String dashboardRoute = "/dashboard";
  @override
  _DashBoardScreenState createState() => _DashBoardScreenState();
}

class _DashBoardScreenState extends State<DashBoardScreen> {
  DateTime selectedDate = DateTime.now();
  DashboardConstants dashboardConstants = new DashboardConstants();

  Map<String, dynamic> todayStatusResponse;

  SharedPref sharedPref = new SharedPref();
  Constants constants;
  bool isProcess = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    constants = new Constants(context);
    if (todayStatusResponse == null) {
      getTodayStatus();
    }

    return Scaffold(
      body: SafeArea(
        child: isProcess
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(20, 20, 0, 0),
                        child: Text(
                          dashboardConstants.DASHBOARDMATRICS,
                          style: TextStyle(
                              fontSize: 24.0, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                  divider(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(20, 20, 0, 0),
                        child: Text(selectedDate.toString().split(" ")[0],
                            style: TextStyle(fontWeight: FontWeight.bold)),
                      ),
                      Padding(
                          padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
                          child: InkWell(
                              onTap: () => {_selectDate(context)},
                              child: Icon(Icons.calendar_today_outlined)))
                    ],
                  ),
                  divider(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      MenuComponents(
                          menuImagePath: dashboardConstants.HRVIMAGE,
                          isRecorded: todayStatusResponse['data']['hrv'],
                          recordedValue: todayStatusResponse['data']['hrvValue']
                              .toDouble(),
                          menuText: dashboardConstants.HRVTEXT,
                          onTap: () => {
                                todayStatusResponse['data']['hrv']
                                    ? Navigator.pushNamed(context,
                                        TrendChartScreen.trendchartRoute,
                                        arguments: TrendChartArguments(
                                            "HRV Trend", "HRV"))
                                    : Navigator.pushNamed(
                                        context, HRVRecordScreen.hrvrecordroute)
                              }),
                      MenuComponents(
                          menuImagePath: dashboardConstants.RHRIMAGE,
                          isRecorded: todayStatusResponse['data']['rhr'],
                          recordedValue: todayStatusResponse['data']['rhrValue']
                              .toDouble(),
                          menuText: dashboardConstants.RHRTEXT,
                          onTap: () => {
                                todayStatusResponse['data']['rhr']
                                    ? Navigator.pushNamed(context,
                                        TrendChartScreen.trendchartRoute,
                                        arguments: TrendChartArguments(
                                            "RHR Trend", "RHR"))
                                    : Navigator.pushNamed(
                                        context, RHRRecordScreen.rhrrecordRoute)
                              }),
                    ],
                  ),
                  divider(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      MenuComponents(
                          menuImagePath: dashboardConstants.SLEEPIMAGE,
                          isRecorded: todayStatusResponse['data']['stt'],
                          recordedValue: todayStatusResponse['data']['sttValue']
                              .toDouble(),
                          menuText: dashboardConstants.SLEEPTEXT,
                          onTap: () => {
                                todayStatusResponse['data']['stt']
                                    ? Navigator.pushNamed(context,
                                        TrendChartScreen.trendchartRoute,
                                        arguments: TrendChartArguments(
                                            "Total Sleep Time Trend", "STT"))
                                    : Navigator.pushNamed(context,
                                        SleepRecordScreen.sleeprecordRoute)
                              }),
                      MenuComponents(
                          menuImagePath: dashboardConstants.STRESSQIMAGE,
                          isRecorded: todayStatusResponse['data']['dsq'],
                          recordedValue: todayStatusResponse['data']['dsqValue']
                              .toDouble(),
                          menuText: dashboardConstants.STRESSQTEXT,
                          onTap: () => {
                                todayStatusResponse['data']['dsq']
                                    ? Navigator.pushNamed(context,
                                        TrendChartScreen.trendchartRoute,
                                        arguments: TrendChartArguments(
                                            "Daily Stress Question Trend",
                                            "DSQ"))
                                    : Navigator.pushNamed(context,
                                        StressSubjectiveQScreen.stresssubjRoute)
                              }),
                    ],
                  ),
                  divider(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      MenuComponents(
                          menuImagePath: dashboardConstants.REMEDIATIONIMAGE,
                          isRecorded: false,
                          recordedValue: 0,
                          menuText: dashboardConstants.REMEDIATIONTEXT,
                          onTap: () => {
                                Navigator.pushNamed(
                                    context, RemediationScreen.remediationRoute)
                              }),
                      MenuComponents(
                        menuImagePath: dashboardConstants.STRESSSTATUSIMAGE,
                        isRecorded: true,
                        recordedValue: todayStatusResponse['data']
                                ['stressStatusValue']
                            .toDouble(),
                        menuText: dashboardConstants.STRESSSTATUSTEXT,
                        onTap: () => {
                          Navigator.pushNamed(
                              context, TrendChartScreen.trendchartRoute,
                              arguments:
                                  TrendChartArguments("Stress Status", "SS"))
                        },
                        bColor: todayStatusResponse['data']['stressStatus'],
                      ),
                    ],
                  ),
                ],
              ),
      ),
    );
  }

  Widget divider() {
    Constants globalConstants = new Constants(context);
    return Divider(
      height: globalConstants.screenHeight * 0.03,
      color: Colors.transparent,
    );
  }

  Future<Null> _selectDate(
    BuildContext context,
  ) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1901, 1),
        lastDate: DateTime.now());
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        getTodayStatus(picked.toIso8601String());
      });
  }

  Future<void> getTodayStatus([String date]) async {
    await sharedPref.initSF();
    String id = sharedPref.getParticipateIdtoSF();
    date ??= DateTime.now().toIso8601String();
    todayStatusResponse = await constants.postApiCallback(
        ApiConstants.todayStatusUrl,
        {"participateId": int.parse(id), "todayDate": date});

    print(todayStatusResponse.toString());

    setState(() {
      isProcess = false;
    });
  }
}
