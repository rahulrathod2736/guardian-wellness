import 'package:injectable/injectable.dart';

@injectable
class HRVRecordConstants {
  final String HRVESTIMATION = "HRV";
  final String BPMESTIMATION = "BPM";

  final String FINGERTIP =
      "Cover both the camera and the flash with your finger, Make sure your finger is touching";

  final String RECORDHRV = "Record";

  final String RECORDING = "Recording...";

  final String SAVEHRV = "Save";

  final String CALCULATING = "Calculating...";
}
