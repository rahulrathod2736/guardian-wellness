import 'package:injectable/injectable.dart';

class SleepRecordConstants {
  final String DATEHINT = "Date";
  final String DATELABEL = "Select Date";

  final String SLEEPTIMEHINT = "Sleep hour";
  final String SLEEPTIMELABEL = "Enter Hours of Sleep";

  final String BUTTONLABEL = "Save Sleep Time";
}
