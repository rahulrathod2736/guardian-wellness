import 'package:flutter/material.dart';

class RadioOption extends StatefulWidget {
  final int selectedOptionValue;
  final String optionValue;
  final int value;
  final Function onChangeRadio;

  RadioOption(
      {this.value,
      this.selectedOptionValue,
      this.onChangeRadio,
      this.optionValue});

  @override
  _RadioOptionState createState() => _RadioOptionState();
}

class _RadioOptionState extends State<RadioOption> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: Row(
        children: [
          Radio(
              value: widget.value,
              groupValue: widget.selectedOptionValue,
              onChanged: widget.onChangeRadio),
          Text(widget.optionValue)
        ],
      ),
    );
  }
}
