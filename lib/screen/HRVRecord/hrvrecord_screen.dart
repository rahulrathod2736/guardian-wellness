import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:guardian/screen/Dashboard/dashboard_screen.dart';
import 'package:guardian/screen/HRVRecord/components/chart.dart';
import 'package:guardian/screen/HRVRecord/hrvrecord_constants.dart';
import 'package:guardian/utility/apiconstants.dart';
import 'package:guardian/utility/components/TrendChart/trendchart_screen.dart';
import 'package:guardian/utility/constants.dart';
import 'package:guardian/utility/injection/injection.dart';
import 'package:guardian/utility/sharedPref.dart';
import 'package:camera/camera.dart';
import 'package:wakelock/wakelock.dart';

class HRVRecordScreen extends StatefulWidget {
  static String hrvrecordroute = "/hrvrecord";

  @override
  _HRVRecordScreenState createState() => _HRVRecordScreenState();
}

class _HRVRecordScreenState extends State<HRVRecordScreen> {
  final HRVRecordConstants hrvReordCostants = getIt<HRVRecordConstants>();

  SharedPref sharedPref = new SharedPref();
  Constants constants;

  bool _toggled = false; // toggle button value
  List<SensorValue> _data = List<SensorValue>(); // array to store the values
  CameraController _controller;
  int _bpm = 0; // beats per minute
  double _hrv = 0.0;
  int _fs = 30; // sampling frequency (fps)
  int _windowLen = 30 * 6; // window length to display - 6 seconds
  CameraImage _image; // store the last camera image
  double _avg; // store the average value during calculation
  DateTime _now; // store the now Datetime
  Timer _timer;
  int _timerCounter = 0;

  @override
  Widget build(BuildContext context) {
    constants = new Constants(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Record HRV"),
      ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Expanded(
                flex: 1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Padding(
                        padding: EdgeInsets.all(12),
                        child: ClipRRect(
                          borderRadius: BorderRadius.all(
                            Radius.circular(18),
                          ),
                          child: Stack(
                            fit: StackFit.expand,
                            alignment: Alignment.center,
                            children: <Widget>[
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Container(
                                    alignment: Alignment.center,
                                    padding: EdgeInsets.all(4),
                                    child: Text(
                                      // _toggled
                                      hrvReordCostants.FINGERTIP,
                                      // : "Camera feed will display here",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                  Container(
                                      child: TextButton(
                                          style: ButtonStyle(
                                            backgroundColor:
                                                MaterialStateProperty.all(
                                              Theme.of(context)
                                                  .primaryColor
                                                  .withOpacity(0.9),
                                            ),
                                          ),
                                          onPressed: () => {},
                                          child: Padding(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 20.0,
                                                vertical: 5.0),
                                            child: Text(
                                                _timerCounter.toString() +
                                                    " seconds",
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.bold,
                                                    letterSpacing: 1.1)),
                                          ))),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Center(
                          child: Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Column(
                            children: [
                              Text(
                                hrvReordCostants.BPMESTIMATION,
                                style:
                                    TextStyle(fontSize: 18, color: Colors.grey),
                              ),
                              Text(
                                (_bpm != 0 ? _bpm.toString() : "0"),
                                style: TextStyle(
                                    fontSize: _bpm > 30 && _bpm < 130 ? 32 : 18,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          Column(
                            children: [
                              Text(
                                hrvReordCostants.HRVESTIMATION,
                                style:
                                    TextStyle(fontSize: 18, color: Colors.grey),
                              ),
                              Text(
                                (_hrv > 0.0 && _hrv < 20.0
                                    ? _hrv.toString()
                                    : "0.0"),
                                style: TextStyle(
                                    fontSize:
                                        _hrv > 0.0 && _hrv < 20.0 ? 32 : 18,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ],
                      )),
                    ),
                  ],
                )),
            Expanded(
              flex: 1,
              child: Container(
                margin: EdgeInsets.all(12),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(18),
                    ),
                    color: Colors.black),
                // child: null
                child: Chart(_data),
              ),
            ),
            Expanded(
              flex: 1,
              child: Center(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(
                            Theme.of(context).primaryColor.withOpacity(0.9),
                          ),
                        ),
                        onPressed: () => {
                              if (!_toggled) {_toggle()}
                            },
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 20.0, vertical: 5.0),
                          child: Text(
                              _toggled
                                  ? hrvReordCostants.RECORDING
                                  : hrvReordCostants.RECORDHRV,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  letterSpacing: 1.1)),
                        )),
                  ),
                  Visibility(
                    visible: _timerCounter == 60 ? true : false,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextButton(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all(
                              Theme.of(context).primaryColor.withOpacity(0.9),
                            ),
                          ),
                          onPressed: () => {
                                SetHRVRecord()
                                // Navigator.pushNamed(
                                //     context, TrendChartScreen.trendchartRoute,
                                //     arguments: TrendChartArguments("HRV Trend"))
                              },
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 20.0, vertical: 5.0),
                            child: Text(hrvReordCostants.SAVEHRV,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    letterSpacing: 1.1)),
                          )),
                    ),
                  ),
                ],
              )),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _timer?.cancel();
    _toggled = false;
    _disposeController();
    Wakelock.disable();
    super.dispose();
  }

  void _disposeController() {
    _controller?.dispose();
    _controller = null;
  }

  void _clearData() {
    // create array of 128 ~= 255/2
    _data.clear();
    int now = DateTime.now().millisecondsSinceEpoch;
    for (int i = 0; i < _windowLen; i++)
      _data.insert(
          0,
          SensorValue(
              DateTime.fromMillisecondsSinceEpoch(now - i * 1000 ~/ _fs), 128));
  }

  void _toggle() {
    _timerCounter = 0;
    _clearData();
    _initController().then((onValue) {
      Wakelock.enable();
      setState(() {
        _toggled = true;
      });
      // after is toggled
      _initTimer();
      _updateBPM();
    });
  }

  void _untoggle() {
    _disposeController();
    Wakelock.disable();

    setState(() {
      _toggled = false;
    });
  }

  Future<void> _initController() async {
    try {
      List _cameras = await availableCameras();
      _controller = CameraController(_cameras.first, ResolutionPreset.low);
      await _controller.initialize();
      Future.delayed(Duration(milliseconds: 100)).then((onValue) {
        _controller.setFlashMode(FlashMode.torch);
      });
      _controller.startImageStream((CameraImage image) {
        _image = image;
      });
    } catch (Exception) {
      debugPrint(Exception);
    }
  }

  void _initTimer() {
    int i = 0;

    _timer = Timer.periodic(Duration(milliseconds: 1000 ~/ _fs), (timer) {
      i++;
      setState(() {
        _timerCounter = ((i * 30) / 1000).floor().toInt();
      });

      if (i == 2000) {
        timer.cancel();
        _untoggle();
      } else {
        if (_toggled) {
          if (_image != null) _scanImage(_image);
        } else {
          timer.cancel();
        }
      }
    });
  }

  void _scanImage(CameraImage image) {
    _now = DateTime.now();
    _avg =
        image.planes.first.bytes.reduce((value, element) => value + element) /
            image.planes.first.bytes.length;
    if (_data.length >= _windowLen) {
      _data.removeAt(0);
    }
    setState(() {
      _data.add(SensorValue(_now, _avg));
    });
  }

  void _updateBPM() async {
    List<SensorValue> _values;
    double _avg;
    int _n;
    double _m;
    double _threshold;
    double _bpm;
    int _counter;
    int _previous;
    int _square = 0;
    List<int> interval = [];
    while (_toggled) {
      _values = List.from(_data);
      _avg = 0;
      _n = _values.length;
      _m = 0;
      _values.forEach((SensorValue value) {
        _avg += value.value / _n;
        if (value.value > _m) _m = value.value;
      });
      _threshold = (_m + _avg) / 2;
      _bpm = 0;
      _counter = 0;
      _previous = 0;

      for (int i = 1; i < _n; i++) {
        if (_values[i - 1].value < _threshold &&
            _values[i].value > _threshold) {
          if (_previous != 0) {
            _counter++;
            interval.add(
                (_values[i].time.millisecondsSinceEpoch - _previous).toInt());
          }
          _previous = _values[i].time.millisecondsSinceEpoch;
        }
      }

      double avgtime = 0;
      for (int i = 1; i < interval.length; i++) {
        _square += pow((interval[i - 1] - interval[i]), 2);
        avgtime += interval[i] / interval.length;
      }
      double sqavg = _square / interval.length;

      double bpmCount = (60 * (1000 / avgtime));

      double hrv = pow(sqavg, 0.5);

      double hrvln = log(hrv);

      if (!hrvln.isNaN &&
          !hrvln.isInfinite &&
          !bpmCount.isNaN &&
          !bpmCount.isInfinite &&
          !(bpmCount > 150 || bpmCount < 30)) {
        if (_counter > 0) {
          setState(() {
            this._bpm = bpmCount.toInt();
            this._hrv = double.parse(hrvln.toStringAsFixed(2));
          });
        }
      }

      await Future.delayed(Duration(
          milliseconds:
              1000 * _windowLen ~/ _fs)); // wait for a new set of _data values
    }
  }

  SetHRVRecord() async {
    await sharedPref.initSF();
    String id = sharedPref.getParticipateIdtoSF();
    String now = new DateTime.now().toIso8601String();
    print("412" + this._hrv.toStringAsFixed(2));
    Map<String, dynamic> responseData =
        await constants.postApiCallback(ApiConstants.saveHrvUrl, {
      "hrvValue": double.parse(this._hrv.toStringAsFixed(2)),
      "participateId": int.parse(id),
      "hrvDate": now
    });
    Map<String, dynamic> response = await constants.postApiCallback(
        ApiConstants.saveRhrUrl, {
      "rhrValue": this._bpm,
      "participateId": int.parse(id),
      "rhrDate": now
    });
    if (responseData['status'] == "200" && response['status'] == "200") {
      Fluttertoast.showToast(
          msg: "RHR & HRV Saved Successfully",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.black,
          textColor: Colors.white,
          fontSize: 16.0);
      Navigator.pushNamed(context, DashBoardScreen.dashboardRoute);
    } else {
      Fluttertoast.showToast(
          msg: responseData['message'],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }
}

class SensorValue {
  final DateTime time;
  final double value;

  SensorValue(this.time, this.value);
}
