class DashboardConstants {
  final String DASHBOARDMATRICS = "Daily Metrics Dashboard";

  // hrv menu text & image
  final String HRVTEXT = "Heart Rate Variability";
  final String HRVIMAGE = "assets/images/heart-rate.png";

  // rhr menu text & image
  final String RHRTEXT = "Resting Heart Rate";
  final String RHRIMAGE = "assets/images/pulse.png";

  // sleep menu text & image
  final String SLEEPTEXT = "Sleep Total Time";
  final String SLEEPIMAGE = "assets/images/sleep.png";

  // stress menu text & image
  final String STRESSQTEXT = "Subjective Stress Score";
  final String STRESSQIMAGE = "assets/images/stress.png";

  // remediation menu text & image
  final String REMEDIATIONTEXT = "Remediation";
  final String REMEDIATIONIMAGE = "assets/images/meditation.png";

  // stress status menu text & image
  final String STRESSSTATUSTEXT = "Stress Status";
  final String STRESSSTATUSIMAGE = "assets/images/stress-management.png";
}
