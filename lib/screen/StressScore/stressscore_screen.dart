import 'package:flutter/material.dart';
import 'package:guardian/screen/Dashboard/dashboard_screen.dart';
import 'package:guardian/screen/SubjectiveStressQ/stresssubjque_screen.dart';
import 'package:guardian/utility/components/TrendChart/trendchart_screen.dart';
import 'package:guardian/utility/constants.dart';

class StressScoreScreen extends StatefulWidget {
  static String stressscoreRoute = "/stressscore";

  @override
  _StressScoreScreenState createState() => _StressScoreScreenState();
}

class _StressScoreScreenState extends State<StressScoreScreen> {
  List<StressQuestion> questionList = [];
  int totalScore = 0;
  Constants constants;
  @override
  Widget build(BuildContext context) {
    questionList =
        ModalRoute.of(context).settings.arguments as List<StressQuestion>;
    constants = new Constants(context);
    return Scaffold(
      appBar: AppBar(
          title: Text("Stress Subjective Score"),
          leading: IconButton(
              icon: Icon(Icons.home_rounded),
              onPressed: () => {
                    Navigator.pushNamed(context, DashBoardScreen.dashboardRoute)
                  })),
      body: SafeArea(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          DataTable(columns: [
            DataColumn(label: Text("Question")),
            DataColumn(label: Text("Option")),
            DataColumn(label: Text("Status"))
          ], rows: getRows()),
          Divider(
            height: 10.0,
            color: Colors.transparent,
          ),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              children: [
                Text(
                  "Total Score",
                  style: TextStyle(
                      color: totalScore > 0 && totalScore <= 25
                          ? Colors.red
                          : (totalScore >= 26 && totalScore <= 37
                              ? Colors.yellow
                              : Colors.green),
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0),
                ),
                Text(totalScore.toString(),
                    style: TextStyle(
                        color: totalScore > 0 && totalScore <= 25
                            ? Colors.red
                            : (totalScore >= 26 && totalScore <= 37
                                ? Colors.yellow
                                : Colors.green),
                        fontWeight: FontWeight.bold,
                        fontSize: 24.0)),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(
                    Theme.of(context).primaryColor.withOpacity(0.9),
                  ),
                ),
                onPressed: () => {
                      Navigator.pushNamed(
                          context, TrendChartScreen.trendchartRoute,
                          arguments:
                              TrendChartArguments("Stress Score Trend", "DSQ"))
                    },
                child: Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
                  child: Text("Show Stress Record",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 1.1)),
                )),
          ),
        ],
      )),
    );
  }

  List<DataRow> getRows() {
    List<DataRow> rows = <DataRow>[];
    questionList.asMap().forEach((index, element) {
      int getQuestionScore = constants.getScore(element);
      totalScore += getQuestionScore;
      rows.add(DataRow(cells: [
        DataCell(Text((index + 1).toString())),
        DataCell(Text((element.selctedvalue + 1).toString())),
        DataCell(Text(getScoreScale(getQuestionScore)))
      ]));
    });

    return rows;
  }

  String getScoreScale(int getQuestionScore) {
    String scale = "";
    switch (getQuestionScore) {
      case 1:
        scale = "Strongly Disagree";
        break;
      case 2:
        scale = "Disagree";
        break;
      case 3:
        scale = "unsure";
        break;
      case 4:
        scale = "agree";
        break;
      case 5:
        scale = "Strongly Agree";
        break;
    }
    return scale;
  }
}
