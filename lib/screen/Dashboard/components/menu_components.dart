import 'package:flutter/material.dart';
import 'package:guardian/utility/constants.dart';

class MenuComponents extends StatefulWidget {
  final String menuImagePath;
  final bool isRecorded;
  final double recordedValue;
  final String menuText;
  final Function onTap;
  final String bColor;

  MenuComponents(
      {@required this.menuImagePath,
      @required this.isRecorded,
      @required this.recordedValue,
      @required this.menuText,
      @required this.onTap,
      this.bColor});

  @override
  _MenuComponentsState createState() => _MenuComponentsState();
}

class _MenuComponentsState extends State<MenuComponents> {
  Color borderColor, textColor;
  @override
  Widget build(BuildContext context) {
    Constants globalConstants = new Constants(context);

    if (widget.bColor == null) {
      borderColor = Colors.grey.withOpacity(0.5);
      textColor = Colors.black;
    } else {
      borderColor = getColor(widget.bColor);
    }
    return Container(
      width: globalConstants.screenWidth * 0.43,
      height: globalConstants.screenWidth * 0.40,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          border: Border.all(color: borderColor)),
      child: InkWell(
        onTap: widget.onTap,
        child: Padding(
          padding: const EdgeInsets.all(2.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(2.0),
                child: !widget.isRecorded
                    ? Image.asset(
                        widget.menuImagePath,
                        width: globalConstants.screenWidth * 0.15,
                        height: globalConstants.screenWidth * 0.15,
                        fit: BoxFit.cover,
                      )
                    : Text(widget.recordedValue.toString(),
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 26.0,
                            color: textColor)),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  widget.menuText,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Color getColor(String bColor) {
    Color color;
    if (bColor == "Red") {
      color = Colors.red;
       textColor = Colors.red;
    } else if (bColor == "Green") {
      color = Colors.green;
      textColor = Colors.green;
    } else if (bColor == "Yellow") {
      color = Colors.yellow;
      textColor = Colors.yellow;
    } else {
      color = Colors.grey.withOpacity(0.5);
      textColor = Colors.black;
    }
    return color;
  }
}
