import 'package:injectable/injectable.dart';

@injectable
class ParticipateConstants {
  //hint text for participaion id Text Field
  final String PIDHINT = "Enter Participation Id";

  //label text for participaion id Text Field
  final String PIDLABEL = "Participation Id";

  //hint text for Age Text Field
  final String AGEHINT = "Enter Age Between 14-99";

  //label text forage  Text Field
  final String AGELABEL = "Age";

  //label for Button
  final String BUTTONLABEL = "Login";

  //image path for participate Screen
  final String GUARDIANIMAGE = "assets/images/logo-square-bg.png";
}
