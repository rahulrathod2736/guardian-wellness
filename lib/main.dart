import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:guardian/screen/HRVRecord/hrvrecord_screen.dart';
import 'package:guardian/screen/Participate/participate_screen.dart';
import 'package:guardian/screen/Dashboard/dashboard_screen.dart';
import 'package:guardian/screen/RHRRecord/rhrrecord_screen.dart';
import 'package:guardian/screen/Remediation/remediation_screen.dart';
import 'package:guardian/screen/SleepRecord/sleeprecord_screen.dart';
import 'package:guardian/screen/StressScore/stressscore_screen.dart';
import 'package:guardian/utility/components/TrendChart/trendchart_screen.dart';
import 'package:guardian/utility/connectivity.dart';
import 'package:guardian/utility/injection/injection.dart';
import 'screen/SubjectiveStressQ/stresssubjque_screen.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  configureDependencies();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Map _source = {ConnectivityResult.mobile: false};
  MyConnectivity _connectivity = MyConnectivity.instance;

  @override
  void initState() {
    super.initState();
    _connectivity.initialise();
    _connectivity.myStream.listen((source) {
      setState(() => _source = source);
    });
  }

  @override
  void dispose() {
    _connectivity.disposeStream();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    switch (_source.keys.toList()[0]) {
      case ConnectivityResult.none:
        Fluttertoast.showToast(
            msg: 'No internet',
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.black,
            textColor: Colors.white,
            fontSize: 16.0);
        break;
    }
    return MaterialApp(
        title: 'BioPhysics',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          fontFamily: 'poppins',
          primaryColor: MaterialColor(0xFF131920, primaryswatch),
        ),
        initialRoute: Participate.particpateRoute,
        routes: {
          Participate.particpateRoute: (context) => Participate(),
          DashBoardScreen.dashboardRoute: (context) => DashBoardScreen(),
          HRVRecordScreen.hrvrecordroute: (context) => HRVRecordScreen(),
          StressSubjectiveQScreen.stresssubjRoute: (context) =>
              StressSubjectiveQScreen(),
          StressScoreScreen.stressscoreRoute: (context) => StressScoreScreen(),
          SleepRecordScreen.sleeprecordRoute: (context) => SleepRecordScreen(),
          RHRRecordScreen.rhrrecordRoute: (context) => RHRRecordScreen(),
          TrendChartScreen.trendchartRoute: (context) => TrendChartScreen(),
          RemediationScreen.remediationRoute: (context) => RemediationScreen(),
        });
  }
}

Map<int, Color> primaryswatch = {
  50: Color.fromRGBO(19, 25, 32, .1),
  100: Color.fromRGBO(19, 25, 32, .2),
  200: Color.fromRGBO(19, 25, 32, .3),
  300: Color.fromRGBO(19, 25, 32, .4),
  400: Color.fromRGBO(19, 25, 32, .5),
  500: Color.fromRGBO(19, 25, 32, .6),
  600: Color.fromRGBO(19, 25, 32, .7),
  700: Color.fromRGBO(19, 25, 32, .8),
  800: Color.fromRGBO(19, 25, 32, .9),
  900: Color.fromRGBO(19, 25, 32, 1),
};
