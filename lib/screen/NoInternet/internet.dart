import 'package:flutter/material.dart';

class Internet extends StatefulWidget {
  static String internetRoute = "/internet";
  @override
  _InternetScreenState createState() => _InternetScreenState();
}

class _InternetScreenState extends State<Internet> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            child: Center(
      child: Text(
        'No Internet',
        style: TextStyle(fontSize: 26),
      ),
    )));
  }
}
