class ApiConstants {
  static final String apiDomain = "guardian-test.us-east-2.elasticbeanstalk.com";
  static final String baseUrl = "api";
  static final String loginUrl = baseUrl + "/Authenticate/Login";
  static final String todayStatusUrl = baseUrl + "/Common/CheckTodayStatus";
  static final String viewChartUrl = baseUrl + "/Common/ViewChartData";

  static final String saveDsqUrl = baseUrl + "/DSQ/SaveDSQ";
  static final String saveHrvUrl = baseUrl + "/HRV/SaveHRV";
  static final String saveRhrUrl = baseUrl + "/RHR/SaveRHR";
  static final String saveSttUrl = baseUrl + "/STT/SaveSTT";
  
}
