import 'package:injectable/injectable.dart';
import 'package:guardian/screen/SubjectiveStressQ/stresssubjque_screen.dart';

@injectable
class StressSubjConstants {
  final String SUBJSTRESS = "Subjective Stress";

  final List<StressQuestion> QUESTIONLIST = [
    StressQuestion(
        "I had a good sleep last night",
        Option("Strongly Agree", 5),
        Option("Agree", 4),
        Option("Unsure", 3),
        Option("Disagree", 2),
        Option("Strongly Disagree", 1),
        -1),
    StressQuestion(
        "I feel well rested today",
        Option("Strongly Agree", 5),
        Option("Agree", 4),
        Option("Unsure", 3),
        Option("Disagree", 2),
        Option("Strongly Disagree", 1),
        -1),
    StressQuestion(
        "I am confident that I will reach my goals today",
        Option("Strongly Agree", 5),
        Option("Agree", 4),
        Option("Unsure", 3),
        Option("Disagree", 2),
        Option("Strongly Disagree", 1),
        -1),
    StressQuestion(
        "I do not feel stress today",
        Option("Strongly Agree", 5),
        Option("Agree", 4),
        Option("Unsure", 3),
        Option("Disagree", 2),
        Option("Strongly Disagree", 1),
        -1),
    StressQuestion(
        "I am not feeling anxious today",
        Option("Strongly Agree", 5),
        Option("Agree", 4),
        Option("Unsure", 3),
        Option("Disagree", 2),
        Option("Strongly Disagree", 1),
        -1),
    StressQuestion(
        "I do not feel distracted today",
        Option("Strongly Agree", 5),
        Option("Agree", 4),
        Option("Unsure", 3),
        Option("Disagree", 2),
        Option("Strongly Disagree", 1),
        -1),
    StressQuestion(
        "I feel motivated today",
        Option("Strongly Agree", 5),
        Option("Agree", 4),
        Option("Unsure", 3),
        Option("Disagree", 2),
        Option("Strongly Disagree", 1),
        -1),
    StressQuestion(
        "I do not feel tired today",
        Option("Strongly Agree", 5),
        Option("Agree", 4),
        Option("Unsure", 3),
        Option("Disagree", 2),
        Option("Strongly Disagree", 1),
        -1),
    StressQuestion(
        "My mind is not wandering today",
        Option("Strongly Agree", 5),
        Option("Agree", 4),
        Option("Unsure", 3),
        Option("Disagree", 2),
        Option("Strongly Disagree", 1),
        -1),
    StressQuestion(
        "I do not have thoughts of personal worries today",
        Option("Strongly Agree", 5),
        Option("Agree", 4),
        Option("Unsure", 3),
        Option("Disagree", 2),
        Option("Strongly Disagree", 1),
        -1),
  ];
}
