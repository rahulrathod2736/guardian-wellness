import 'package:flutter/material.dart';
import 'package:guardian/screen/Dashboard/dashboard_screen.dart';
import 'package:guardian/screen/Remediation/remediation_constants.dart';
import 'package:guardian/utility/injection/injection.dart';

class RemediationScreen extends StatefulWidget {
  static String remediationRoute = "/remediation";
  @override
  _RemediationScreenState createState() => _RemediationScreenState();
}

class _RemediationScreenState extends State<RemediationScreen> {
  final RemediationConstants remediationConstants =
      getIt<RemediationConstants>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            child: SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child:Row(
            children: [
              IconButton(
              icon: Icon(Icons.home_rounded,size: 28),
              onPressed: () => {
                    Navigator.pushNamed(context, DashBoardScreen.dashboardRoute)
                  }),
              Text(
              remediationConstants.REMEDIATION,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24.0),
            ),
            ],
          ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: defination(remediationConstants.REMDEF),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ExpansionTile(
                title: Text(remediationConstants.MEDITEXT,
                    style: TextStyle(fontWeight: FontWeight.bold)),
                children: [
                  defination(remediationConstants.MEDDEF),
                  Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(remediationConstants.MEDDESC))
                ]),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ExpansionTile(
                title: Text(remediationConstants.BREATEXT,
                    style: TextStyle(fontWeight: FontWeight.bold)),
                children: [
                  defination(remediationConstants.BREDEF),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(remediationConstants.BREADESC,
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  breathWorkMethod(remediationConstants.BREAMNAME1,
                      remediationConstants.BREAMDESC1),
                  breathWorkMethod(remediationConstants.BREAMNAME2,
                      remediationConstants.BREAMDESC2),
                  breathWorkMethod(remediationConstants.BREAMNAME3,
                      remediationConstants.BREAMDESC3),
                ]),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ExpansionTile(
                title: Text(remediationConstants.EXERTEXT,
                    style: TextStyle(fontWeight: FontWeight.bold)),
                children: [
                  defination(remediationConstants.EXEDEF),
                  Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(remediationConstants.EXERDESC))
                ]),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ExpansionTile(
                title: Text(remediationConstants.SLEETEXT,
                    style: TextStyle(fontWeight: FontWeight.bold)),
                children: [
                  defination(remediationConstants.EXEDEF),
                  sleepHygieneMethod(remediationConstants.SLEEM1NAME +
                      remediationConstants.SLEEM1DESC),
                  sleepHygieneMethod(remediationConstants.SLEEM2NAME +
                      remediationConstants.SLEEM2DESC),
                  sleepHygieneMethod(remediationConstants.SLEEM3NAME +
                      remediationConstants.SLEEM3DESC),
                  sleepHygieneMethod(remediationConstants.SLEEM4NAME +
                      remediationConstants.SLEEM4DESC),
                ]),
          )
        ],
      ),
    )));
  }

  breathWorkMethod(String name, String method) {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(name, style: TextStyle(fontWeight: FontWeight.bold)),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(method),
            ),
          ],
        ));
  }

  defination(String s) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(s, style: TextStyle(fontWeight: FontWeight.bold)),
    );
  }

  // sleepHygieneMethod(String name, String desc) {
  //   return Padding(
  //       padding: const EdgeInsets.all(8.0),
  //       child: RichText(
  //           text: TextSpan(
  //               style: TextStyle(color: Colors.black, fontSize: 18.0),
  //               children: [
  //             TextSpan(
  //                 text: name, style: TextStyle(fontWeight: FontWeight.bold)),
  //             TextSpan(text: desc)
  //           ])));
  // }
  sleepHygieneMethod(String desc) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(desc),
    );
  }
}
