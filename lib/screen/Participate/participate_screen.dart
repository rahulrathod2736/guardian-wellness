import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:guardian/utility/constants.dart';
import 'package:guardian/screen/Dashboard/dashboard_screen.dart';
import 'package:guardian/screen/HRVRecord/hrvrecord_screen.dart';
import 'package:guardian/screen/Participate/participate_constants.dart';
import 'package:guardian/utility/apiconstants.dart';
import 'package:guardian/utility/injection/injection.dart';
import 'package:guardian/utility/sharedPref.dart';
import 'package:http/http.dart' as http;

class Participate extends StatefulWidget {
  static String particpateRoute = "/participate";
  @override
  _ParticipateState createState() => _ParticipateState();
}

class _ParticipateState extends State<Participate> {
  final ParticipateConstants partcipateConstants =
      getIt<ParticipateConstants>();

  Constants constants;

  TextEditingController partcipateidController = new TextEditingController();
  TextEditingController ageController = new TextEditingController();
  SharedPref sharedPref = new SharedPref();
  bool isProcess = true;
  bool isAgeError = false;
  String _selected = "Sex";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    checkParticipateID();
  }

  @override
  Widget build(BuildContext context) {
    constants = new Constants(context);
    return Scaffold(
      backgroundColor: Color(0xFF253141),
      body: SafeArea(
        child: isProcess
            ? Center(child: CircularProgressIndicator())
            : SingleChildScrollView(
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Divider(
                        height: MediaQuery.of(context).size.height * 0.04,
                        color: Colors.transparent,
                      ),
                      Image.asset(
                        partcipateConstants.GUARDIANIMAGE,
                        width: 180.0,
                        height: 180.0,
                        fit: BoxFit.cover,
                      ),
                      // ),
                      Divider(
                        height: MediaQuery.of(context).size.height * 0.13,
                        color: Colors.transparent,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20.0),
                        child: TextField(
                            autofocus: false,
                            controller: partcipateidController,
                            decoration: InputDecoration(
                              fillColor: Colors.white,
                              filled: true,
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white)),
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white)),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white)),
                              hintText: partcipateConstants.PIDHINT,
                              // labelText: partcipateConstants.PIDLABEL,
                            ),
                            style: TextStyle(
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold,
                                letterSpacing: 1.1)),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20.0, vertical: 10.0),
                        child: TextField(
                            autofocus: false,
                            controller: ageController,
                            decoration: InputDecoration(
                              errorText: isAgeError
                                  ? "Please Enter Age Between 14-99"
                                  : null,
                              fillColor: Colors.white,
                              filled: true,
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white)),
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white)),
                              border: OutlineInputBorder(),
                              hintText: partcipateConstants.AGEHINT,
                              // labelText: partcipateConstants.AGELABEL,
                            ),
                            style: TextStyle(
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold,
                                letterSpacing: 1.1)),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20.0, vertical: 10.0),
                        child: InputDecorator(
                          decoration: InputDecoration(
                            fillColor: Colors.white,
                            filled: true,
                            hintText: 'Choose Sex',
                            // labelText: 'Sex',
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white)),
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white)),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                              value: _selected,
                              isDense: true,
                              underline: SizedBox(),
                              onChanged: (String newValue) {
                                setState(() {
                                  _selected = newValue;
                                });
                              },
                              hint: Text(
                                "Please choose Sex",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500),
                              ),
                              items: ["Sex", "Male", "Female", "Not Specified"]
                                  .map((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value,
                                      style: TextStyle(
                                          fontSize: 18.0,
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                          letterSpacing: 1.1)),
                                );
                              }).toList(),
                            ),
                          ),
                        ),
                      ),
                      Divider(
                        height: 30.0,
                        color: Colors.transparent,
                      ),
                      TextButton(
                          onPressed: () => {
                                if (ageController.text.isNotEmpty)
                                  {
                                    if (int.parse(ageController.text) < 14 ||
                                        int.parse(ageController.text) > 99)
                                      {
                                        setState(() {
                                          isAgeError = true;
                                        })
                                      }
                                    else
                                      {
                                        storePraticipateId(
                                            partcipateidController.text,
                                            _selected,
                                            ageController.text)
                                      }
                                  }
                                else
                                  {
                                    Fluttertoast.showToast(
                                        msg: 'Please enter age',
                                        toastLength: Toast.LENGTH_LONG,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        backgroundColor: Colors.white,
                                        textColor: Colors.black,
                                        fontSize: 16.0)
                                  }
                              },
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 30.0, vertical: 5.0),
                            child: Text(partcipateConstants.BUTTONLABEL,
                                style: TextStyle(
                                    fontSize: 18.0,
                                    color: Theme.of(context)
                                        .primaryColor
                                        .withOpacity(0.9),
                                    fontWeight: FontWeight.bold,
                                    letterSpacing: 1.1)),
                          ),
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.white),
                          )),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  storePraticipateId(String id, String gender, String age) async {
    var url = Uri.http(ApiConstants.apiDomain, "api/Authenticate/Login");
    if (id.isNotEmpty && gender != null) {
      if (gender == "Sex") {
        Fluttertoast.showToast(
            msg: 'Please select gender',
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.white,
            textColor: Colors.black,
            fontSize: 16.0);
      } else {
        var response = await http.post(
          url,
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(<String, dynamic>{
            "participateId": id,
            "gender": gender,
            "age": int.parse(age)
          }),
        );
        if (response.statusCode == 200) {
          final jsonData = json.decode(response.body);
          sharedPref.setParticipateIdtoSF(
              jsonData['data']['id'].toString(), jsonData['token']);
          checkTodayStatus(jsonData['data']['id'].toString());
        } else {
          Fluttertoast.showToast(
              msg: 'Something went wrong',
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.white,
              textColor: Colors.black,
              fontSize: 16.0);
        }
      }
    } else {
      Fluttertoast.showToast(
          msg: 'Please fill all the details',
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.white,
          textColor: Colors.black,
          fontSize: 16.0);
    }
  }

  void checkParticipateID() async {
    await sharedPref.initSF();
    String id = sharedPref.getParticipateIdtoSF();
    if (id != '-1') {
      checkTodayStatus(id);
    } else {
      setState(() {
        isProcess = false;
      });
    }
  }

  void checkTodayStatus(String id) async {
    String now = new DateTime.now().toIso8601String();
    Map<String, dynamic> responseData = await constants.postApiCallback(
        ApiConstants.todayStatusUrl,
        {"participateId": int.parse(id), "todayDate": now});
    checkHRVStatus(responseData['data']['hrv']);
  }

  void checkHRVStatus(bool hrv) {
    if (!hrv) {
      navigateTo(HRVRecordScreen.hrvrecordroute);
    } else {
      navigateTo(DashBoardScreen.dashboardRoute);
    }
  }

  void navigateTo(String route) {
    Navigator.pushReplacementNamed(context, route);
  }
}
