// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart';
import 'package:guardian/screen/Dashboard/dashboard_constants.dart';
import 'package:guardian/screen/HRVRecord/hrvrecord_constants.dart';
import 'package:guardian/screen/Participate/participate_constants.dart';
import 'package:guardian/screen/RHRRecord/rhrrecord_constants.dart';
import 'package:guardian/screen/Remediation/remediation_constants.dart';
import 'package:guardian/screen/SleepRecord/sleeprecord_constants.dart';
import 'package:guardian/screen/SubjectiveStressQ/stresssubjque_constants.dart';
import 'package:guardian/utility/components/TrendChart/trendchart_constants.dart';

void $initGetIt(GetIt g, {String environment}) {
  g.registerFactory<ParticipateConstants>(() => ParticipateConstants());
  g.registerFactory<TrendChartConstants>(() => TrendChartConstants());
  g.registerFactory<DashboardConstants>(() => DashboardConstants());
  g.registerFactory<HRVRecordConstants>(() => HRVRecordConstants());
  g.registerFactory<StressSubjConstants>(() => StressSubjConstants());
  g.registerFactory<SleepRecordConstants>(() => SleepRecordConstants());
  g.registerFactory<RHRRecordConstants>(() => RHRRecordConstants());
  g.registerFactory<RemediationConstants>(() => RemediationConstants());
}
