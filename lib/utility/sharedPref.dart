import 'package:shared_preferences/shared_preferences.dart';

class SharedPref {
  SharedPreferences pref;

  initSF() async {
    pref = await SharedPreferences.getInstance();
  }

  setParticipateIdtoSF(String id, String token) {
    pref.setString("participateid", id);
    pref.setString("token", token);
  }

  getParticipateIdtoSF() {
    String participateid = pref.getString("participateid") ?? '-1';
    return participateid;
  }

  getParticipateTokentoSF() {
    String participateToken = pref.getString("token") ?? "";
    return participateToken;
  }

  isTodayHRV() {
    bool istodayhrv = pref.getBool("istodayhrv") ?? false;
    return istodayhrv;
  }

  setTodayHRV(bool isTodayRecord, double hrv, int bpm) {
    pref.setBool("istodayhrv", isTodayRecord);
    pref.setDouble("todayHRV", hrv);
    pref.setBool("istodaybpm", isTodayRecord);
    pref.setInt("todayBPM", bpm);
    print("set Today HRV Record Confirm " + isTodayRecord.toString());
  }
}
