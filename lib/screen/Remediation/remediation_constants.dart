class RemediationConstants {
  final String REMEDIATION = "Re-me-dia-tion";

  final String MEDITEXT = "Meditation";
  final String BREATEXT = "Breath Work";
  final String EXERTEXT = "Excercise";
  final String SLEETEXT = "Sleep Hygiene";

  final String BREADESC =
      "Choose a technique based on how you are feeling today.";
  final String REMDEF =
      "the action of remedying something, in particular of reversing or stopping environmental damage.";
  final String MEDDEF =
      "to think deeply or focus one’s mind for a period of time, in silence or with the aid of chanting, as a method of relaxation.";
  final String BREDEF =
      "take air into the lungs and then expel it, especially as a regular physiological process.";
  final String EXEDEF =
      "activity requiring physical effort, carried out to sustain or improve physical fitness and overall health and wellness.";
  final String SLEDEF =
      "having both a bedroom environment and daily routine that promotes consistent, uninterrupted sleep.";

  final String MEDDESC =
      "1.	Find a comfortable place to sit that feels calm and quiet.\n 2.	Set a time limit. Choose something short (e.g. 5 or 10 mins) to start and gradually increase in small increments.\n 3	Focus your mind on your breath and your body.\n 4.	Check in on a wandering mind and come back to your focus.\n 5.	Be kind and don’t judge yourself. There is no right or wrong.\n 6.	Close on for the effort you made. Notice how your mind and body feel.";

  final String BREAMNAME1 = "4-7-8 to slow & calm the body";
  final String BREAMDESC1 =
      "Empty your lungs of air. Breath in through your nose for 4 seconds, hold your breath for 7 seconds, exhale out of your mouth for 8 seconds. Repeat at least 5 times.";
  final String BREAMNAME2 = "4-4-4-4 for energy & focus";
  final String BREAMDESC2 =
      "Empty your lungs of air and hold your breath for 4 seconds. Breath in through your nose for 4 seconds, hold your breath for 4 seconds, then exhale out of your mouth for 4 seconds. Work your way up to 5 minutes.";
  final String BREAMNAME3 = "5-5 for grounding";
  final String BREAMDESC3 =
      "Focus on the natural rhythm of your breathing. For 1 minute, breath in for 4 seconds and exhale for 4 seconds. Repeat for 5 seconds and then 6 seconds. Gradually work your way up to 10 seconds. Start with 5 minutes and work your way up to 20 minutes.";

  final String EXERDESC =
      "Choose an activity that you enjoy or try something new today. Get outside for a 30 minute walk or jog. Join a virtual or in-person class for peer-to-peer motivation. Increase your heart rate and get your blood flowing.";

  final String SLEEM1NAME = "1) Set a Sleep Schedule : ";
  final String SLEEM1DESC =
      "A fluctuating schedule keeps you from getting  into a rhythm of consistent sleep. Introduce changes slowly (1-2 hours at a time)";
  final String SLEEM2NAME = "2) Create a Routine : ";
  final String SLEEM2DESC =
      "Following the same steps each night reinforces your mind that it’s bedtime. Wind down 30 minutes before bed with activities that calm your mind (e.g. soft music, reading, relaxation exercises. Unplug from electronics 60 minutes before bed to reduce mental stimulation and increase melatonin production";
  final String SLEEM3NAME = "3) Practice Healthy Daily Habits : ";
  final String SLEEM3DESC =
      "Get daylight exposure to drive your circadian rhythms to encourage quality sleep. Exercise regularly to make it easier to sleep at night. Don’t smoke as nicotine stimulates the body in ways that disrupt sleep. Moderate alcohol consumption and avoid it later in the evening. Cut down on caffeine in the afternoon and evening, especially if you are using it as a way to make up for lack of sleep. Don’t dine too late otherwise you’re still digesting when you should be winding down to sleep. Restrict in-bed activity to sleep (and sex being the exception) to create a positive link between sleep and being in bed";
  final String SLEEM4NAME = "4) Optimize your Bedroom : ";
  final String SLEEM4DESC =
      "Create a tranquil environment by blocking out light, downing out noise, and using calming scents. Set a cool but comfortable temperature and ensure you have a comfortable mattress, pillow, and soft sheets.";
}
