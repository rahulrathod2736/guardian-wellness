import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:guardian/screen/Dashboard/dashboard_screen.dart';
import 'package:guardian/screen/SleepRecord/sleeprecord_constants.dart';
import 'package:guardian/utility/apiconstants.dart';
import 'package:guardian/utility/components/TrendChart/trendchart_screen.dart';
import 'package:guardian/utility/constants.dart';
import 'package:guardian/utility/injection/injection.dart';
import 'package:guardian/utility/sharedPref.dart';

class SleepRecordScreen extends StatefulWidget {
  static String sleeprecordRoute = "/sleeprecord";

  @override
  _SleepRecordScreenState createState() => _SleepRecordScreenState();
}

class _SleepRecordScreenState extends State<SleepRecordScreen> {
  DateTime selectedDate = DateTime.now();
  TextEditingController _date;

  SharedPref sharedPref = new SharedPref();
  Constants constants;

  TextEditingController _sleepTime = new TextEditingController();

  final SleepRecordConstants sleepRecordConstants =
      getIt<SleepRecordConstants>();

  @override
  Widget build(BuildContext context) {
    _date = new TextEditingController(text:selectedDate.toString().split(" ")[0]);
    constants = new Constants(context);
    return Scaffold(
        appBar: AppBar(title: Text("Sleep Time")),
        body: SafeArea(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                  keyboardType: TextInputType.text,
                  autofocus: false,
                  controller: _sleepTime,
                  decoration: InputDecoration(
                    focusColor: Theme.of(context).primaryColor,
                    border: OutlineInputBorder(),
                    hintText: sleepRecordConstants.SLEEPTIMEHINT,
                    labelText: sleepRecordConstants.SLEEPTIMELABEL,
                  ),
                  cursorColor: Theme.of(context).primaryColor,
                  style: TextStyle(fontWeight: FontWeight.bold)),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: InkWell(
                onTap: () => {_selectDate(context)},
                child: AbsorbPointer(
                  child: TextField(
                      autofocus: false,
                      keyboardType: TextInputType.datetime,
                      controller: _date,
                      decoration: InputDecoration(
                          focusColor: Theme.of(context).primaryColor,
                          border: OutlineInputBorder(),
                          hintText: sleepRecordConstants.DATEHINT,
                          labelText: sleepRecordConstants.DATELABEL,
                          suffixIcon: InkWell(
                              child: Icon(Icons.calendar_today_rounded))),
                      cursorColor: Theme.of(context).primaryColor,
                      style: TextStyle(fontWeight: FontWeight.bold)),
                ),
              ),
            ),
            TextButton(
                onPressed: () => {SetSTTRecord()},
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 5.0),
                  child: Text(sleepRecordConstants.BUTTONLABEL,
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 1.1)),
                ),
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(
                      Theme.of(context).primaryColor.withOpacity(0.9)),
                )),
          ],
        )));
  }

  Future<Null> _selectDate(
    BuildContext context,
  ) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1901, 1),
        lastDate: DateTime.now());
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        _date.value = TextEditingValue(text: picked.toString().split(" ")[0]);
      });
  }

  SetSTTRecord() async {
    await sharedPref.initSF();

    String id = sharedPref.getParticipateIdtoSF();
    print(double.parse(_sleepTime.text.toString()));
    print(int.parse(id));
    Map<String, dynamic> responseData =
        await constants.postApiCallback(ApiConstants.saveSttUrl, {
      "sttValue": double.parse(_sleepTime.text.toString()),
      "participateId": int.parse(id),
      "sttDate": selectedDate.toIso8601String()
    });
    if (responseData['status'] == "200") {
      Fluttertoast.showToast(
          msg: responseData['message'],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.black,
          textColor: Colors.white,
          fontSize: 16.0);
      Navigator.pushNamed(context, DashBoardScreen.dashboardRoute);
    } else {
      Fluttertoast.showToast(
          msg: responseData['message'],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }
}
