import 'package:flutter/material.dart';
import 'package:guardian/screen/Dashboard/dashboard_screen.dart';
import 'package:guardian/screen/StressScore/stressscore_screen.dart';
import 'package:guardian/screen/SubjectiveStressQ/components/question_components.dart';
import 'package:guardian/screen/SubjectiveStressQ/stresssubjque_constants.dart';
import 'package:guardian/utility/apiconstants.dart';
import 'package:guardian/utility/components/TrendChart/trendchart_screen.dart';
import 'package:guardian/utility/constants.dart';
import 'package:guardian/utility/injection/injection.dart';
import 'package:guardian/utility/sharedPref.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:fluttertoast/fluttertoast.dart';

class StressSubjectiveQScreen extends StatefulWidget {
  static String stresssubjRoute = "/stresssubj";

  @override
  _StressSubjectiveQScreenState createState() =>
      _StressSubjectiveQScreenState();
}

class _StressSubjectiveQScreenState extends State<StressSubjectiveQScreen> {
  List<StressQuestion> questinoList = [];
  int radioBtngValue = -1;
  int selectedAns = 0;
  final _controller = ScrollController();

  final StressSubjConstants stressSubjConstants = getIt<StressSubjConstants>();

  SharedPref sharedPref = new SharedPref();
  Constants constants;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    questinoList = stressSubjConstants.QUESTIONLIST;
  }

  @override
  Widget build(BuildContext context) {
    constants = new Constants(context);
    return Scaffold(
      appBar: AppBar(title: Text(stressSubjConstants.SUBJSTRESS)),
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: LinearPercentIndicator(
                lineHeight: 10.0,
                percent: (selectedAns / 10),
                linearStrokeCap: LinearStrokeCap.roundAll,
                progressColor: Theme.of(context).primaryColor,
                trailing: Text('${selectedAns * 10} %'),
              ),
            ),
            Expanded(
                flex: 1,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: questinoList.length,
                    controller: _controller,
                    itemBuilder: (BuildContext cxt, int index) {
                      void handleRadioButton(int value) {
                        setState(() {
                          questinoList[index].selctedvalue = value;
                        });
                        selectedAns = 0;
                        questinoList.forEach((e) {
                          if (e.selctedvalue > -1) {
                            selectedAns += 1;
                          }
                        });

                        _animateToIndex(index);
                        setState(() {});
                      }

                      return StressQuestionComp(
                        questionList: questinoList,
                        index: index,
                        onChangeRadio: handleRadioButton,
                      );
                    })),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(
                      selectedAns == 10
                          ? Theme.of(context).primaryColor.withOpacity(0.9)
                          : Colors.grey,
                    ),
                  ),
                  onPressed: () => {
                        selectedAns == 10
                            ? SetDSQRecord()
                            : Fluttertoast.showToast(
                                msg: "Please Fill all Question",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.BOTTOM,
                                timeInSecForIosWeb: 1,
                                backgroundColor: Colors.black,
                                textColor: Colors.white,
                                fontSize: 16.0)
                      },
                  child: Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
                    child: Text("Submit",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            letterSpacing: 1.1)),
                  )),
            ),
          ],
        ),
      ),
    );
  }

  _animateToIndex(i) =>
      _controller.animateTo(MediaQuery.of(context).size.width * (i + 1),
          duration: Duration(seconds: 1), curve: Curves.fastOutSlowIn);

  SetDSQRecord() async {
    await sharedPref.initSF();
    String id = sharedPref.getParticipateIdtoSF();
    String now = new DateTime.now().toIso8601String();
    Map<String, dynamic> responseData =
        await constants.postApiCallback(ApiConstants.saveDsqUrl, {
      "question1": constants.getScore(questinoList[0]),
      "question2": constants.getScore(questinoList[1]),
      "question3": constants.getScore(questinoList[2]),
      "question4": constants.getScore(questinoList[3]),
      "question5": constants.getScore(questinoList[4]),
      "question6": constants.getScore(questinoList[5]),
      "question7": constants.getScore(questinoList[6]),
      "question8": constants.getScore(questinoList[7]),
      "question9": constants.getScore(questinoList[8]),
      "question10": constants.getScore(questinoList[9]),
      "totalScore": constants.getTotalScore(questinoList),
      "participateId": int.parse(id),
      "dsqDate": now
    });
    if (responseData['status'] == "200") {
      Fluttertoast.showToast(
          msg: responseData['message'],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.black,
          textColor: Colors.white,
          fontSize: 16.0);
      Navigator.pushNamed(context, DashBoardScreen.dashboardRoute);
    } else {
      Fluttertoast.showToast(
          msg: responseData['message'],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }
}

class StressQuestion {
  String question;
  Option option1;
  Option option2;
  Option option3;
  Option option4;
  Option option5;
  int selctedvalue;
  StressQuestion(this.question, this.option1, this.option2, this.option3,
      this.option4, this.option5, this.selctedvalue);
}

class Option {
  String optiontext;
  int optionScore;

  Option(this.optiontext, this.optionScore);
}
