import 'package:injectable/injectable.dart';

@injectable
class RHRRecordConstants {
  final String BPMESTIMATION = "RHR";

  final String FINGERTIP =
      "Cover both the camera and the flash with your finger, Make sure your finger is touching";

  final String RECORDBPM = "Record";

  final String RECORDING = "Recording...";

  final String SAVEBPM = "Save";

  final String CALCULATING = "Calculating...";
}
