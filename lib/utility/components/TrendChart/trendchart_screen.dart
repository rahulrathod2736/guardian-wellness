import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:guardian/screen/Dashboard/dashboard_screen.dart';
import 'package:guardian/utility/apiconstants.dart';
import 'package:guardian/utility/components/TrendChart/trendchart_constants.dart';
import 'package:guardian/utility/constants.dart';
import 'package:guardian/utility/sharedPref.dart';

class TrendChartScreen extends StatefulWidget {
  static String trendchartRoute = "/trendcharts";

  @override
  _TrendChartScreenState createState() => _TrendChartScreenState();
}

class _TrendChartScreenState extends State<TrendChartScreen> {
  TrendChartConstants trendChartConstants = new TrendChartConstants();

  String _selected = "7 Days";
  TrendChartArguments arguments;
  List<charts.Series<TrendChartData, DateTime>> chartData;
  bool isProcess = true;

  SharedPref sharedPref = new SharedPref();
  Constants constants;

  @override
  Widget build(BuildContext context) {
    arguments =
        ModalRoute.of(context).settings.arguments as TrendChartArguments;
    constants = new Constants(context);
    if (chartData == null) getTrendCartData();
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
          automaticallyImplyLeading: true,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
          elevation: 0.0,
          leading: IconButton(
              color: Colors.black,
              icon: Icon(Icons.home_rounded),
              onPressed: () => {
                    Navigator.pushNamed(context, DashBoardScreen.dashboardRoute)
                  })),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 1,
              child: Center(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Text(
                        arguments.title,
                        style: TextStyle(
                            fontSize: 25.0, fontWeight: FontWeight.bold),
                      ),
                    ),
                    DropdownButton<String>(
                      focusColor: Colors.white,
                      value: _selected,
                      items: ["7 Days", "14 Days", "30 Days"]
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(
                            value,
                            style: TextStyle(color: Colors.black),
                          ),
                        );
                      }).toList(),
                      hint: Text(
                        "Please choose a Days",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                            fontWeight: FontWeight.w500),
                      ),
                      onChanged: (String value) {
                        setState(() {
                          _selected = value;
                          isProcess = true;
                        });
                        getTrendCartData();
                      },
                    ),
                  ],
                ),
              ),
            ),
            divider(),
            Expanded(
              flex: 2,
              child: isProcess
                  ? Center(child: CircularProgressIndicator())
                  : Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                          child: charts.TimeSeriesChart(chartData,
                              animate: true,
                              // primaryMeasureAxis: charts.NumericAxisSpec(
                              //     viewport: charts.NumericExtents(0, 8)),
                              dateTimeFactory:
                                  const charts.LocalDateTimeFactory(),
                              defaultRenderer: new charts.LineRendererConfig(
                                  includePoints: true,
                                  includeArea: true,
                                  stacked: true))),
                    ),
            ),
          ],
        ),
      ),
    );
  }

  getTrendCartData() async {
    await sharedPref.initSF();
    String id = sharedPref.getParticipateIdtoSF();
    String now = new DateTime.now().toIso8601String();
    String startDate = new DateTime.now()
        .subtract(new Duration(days: int.parse(_selected.split(" ")[0])))
        .toIso8601String();
    Map<String, dynamic> responseData =
        await constants.postApiCallback(ApiConstants.viewChartUrl, {
      "participateId": int.parse(id),
      "type": arguments.tag,
      "startDate": startDate,
      "endDate": now
    });

    _createSampleData(responseData);
  }

  _createSampleData(Map<String, dynamic> responseData) {
    List<TrendChartData> data = [];

    for (int i = 0; i < responseData['data'].length; i++) {
      String date = responseData['data'][i]['chartDate'];
      data.add(new TrendChartData(
          new DateTime(int.parse(date.split("/")[2]),
              int.parse(date.split("/")[0]), int.parse(date.split("/")[1])),
          double.parse(double.parse(responseData['data'][i]['value'].toString())
              .toStringAsFixed(2))));
    }

    print(data.toString());

    chartData = [
      new charts.Series<TrendChartData, DateTime>(
        id: 'Trend Chart',
        colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
        domainFn: (TrendChartData data, _) => data.date,
        measureFn: (TrendChartData data, _) => data.value,
        data: data,
      )
    ];
    setState(() {
      isProcess = false;
    });
  }

  Widget divider() {
    Constants globalConstants = new Constants(context);
    return Divider(
      height: globalConstants.screenHeight * 0.1,
      color: Colors.transparent,
    );
  }
}

/// Sample linear data type.
class TrendChartData {
  final DateTime date;
  final double value;

  TrendChartData(this.date, this.value);
}

class TrendChartArguments {
  final String title;

  final String tag;

  TrendChartArguments(this.title, this.tag);
}
